package connection

import (
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels/v3/inbound"
	"git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"git.openprivacy.ca/openprivacy/libricochet-go/policies"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"golang.org/x/crypto/ed25519"
	"sync"
)

// InboundConnectionHandler is a convieniance wrapper for handling inbound
// connections
type InboundConnectionHandler struct {
	connection *Connection
}

// HandleInboundConnection returns an InboundConnectionHandler given a connection
func HandleInboundConnection(c *Connection) *InboundConnectionHandler {
	ich := new(InboundConnectionHandler)
	ich.connection = c
	return ich
}

// ProcessAuthAsV3Server blocks until authentication has succeeded, failed, or the
// connection is closed. A non-nil error is returned in all cases other than successful
// and accepted authentication.
//
// ProcessAuthAsV3Server cannot be called at the same time as any other call to a Process
// function. Another Process function must be called after this function successfully
// returns to continue handling connection events.
//
// The acceptCallback function is called after receiving a valid authentication proof
// with the client's authenticated hostname and public key. acceptCallback must return
// true to accept authentication and allow the connection to continue, and also returns a
// boolean indicating whether the contact is known and recognized. Unknown contacts will
// assume they are required to send a contact request before any other activity.
func (ich *InboundConnectionHandler) ProcessAuthAsV3Server(v3identity identity.Identity, sach func(hostname string, publicKey ed25519.PublicKey) (allowed, known bool)) error {

	var breakOnce sync.Once

	var authAllowed, authKnown bool
	var authHostname string

	onAuthValid := func(hostname string, publicKey ed25519.PublicKey) (allowed, known bool) {
		authAllowed, authKnown = sach(hostname, publicKey)
		if authAllowed {
			authHostname = hostname
		}
		breakOnce.Do(func() { go ich.connection.Break() })
		return authAllowed, authKnown
	}
	onAuthInvalid := func(err error) {
		// err is ignored at the moment
		breakOnce.Do(func() { go ich.connection.Break() })
	}

	ach := new(AutoConnectionHandler)
	ach.Init()
	ach.RegisterChannelHandler("im.ricochet.auth.3dh",
		func() channels.Handler {
			return &inbound.Server3DHAuthChannel{
				ServerIdentity:    v3identity,
				ServerAuthValid:   onAuthValid,
				ServerAuthInvalid: onAuthInvalid,
			}
		})

	// Ensure that the call to Process() cannot outlive this function,
	// particularly for the case where the policy timeout expires
	defer breakOnce.Do(func() { ich.connection.Break() })
	policy := policies.UnknownPurposeTimeout
	err := policy.ExecuteAction(func() error {
		return ich.connection.Process(ach)
	})

	if err == nil {
		if authAllowed == true {
			ich.connection.RemoteHostname = authHostname
			return nil
		}
		return utils.ClientFailedToAuthenticateError
	}

	return err
}
