package connection

import (
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels/v3/inbound"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"github.com/golang/protobuf/proto"
	"testing"
)

// Test sending valid packets
func TestInit(t *testing.T) {
	ach := new(AutoConnectionHandler)
	ach.Init()
	ach.RegisterChannelHandler("im.ricochet.auth.3dh", func() channels.Handler {
		return &inbound.Server3DHAuthChannel{}
	})

	// Construct the Open Authentication Channel Message
	messageBuilder := new(utils.MessageBuilder)
	ocm := messageBuilder.Open3EDHAuthenticationChannel(1, [32]byte{}, [32]byte{})

	// We have just constructed this so there is little
	// point in doing error checking here in the test
	res := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(ocm[:], res)
	opm := res.GetOpenChannel()
	handler, err := ach.OnOpenChannelRequest(opm.GetChannelType())

	if err == nil {
		if handler.Type() != "im.ricochet.auth.3dh" {
			t.Errorf("Failed to authentication handler: %v", handler.Type())
		}
	} else {
		t.Errorf("Failed to build handler: %v", err)
	}

	types := ach.GetSupportedChannelTypes()
	if len(types) != 1 {
		t.Errorf("Expected only im.ricochet.auth.hidden-service to be supported instead got: %v", types)
	}
}
