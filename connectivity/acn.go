package connectivity

import (
	"net"
)

// PrivateKey represents a private key using an unspecified algorithm.
type PrivateKey interface{}

// ListenService is an address that was opened with Listen() and can Accept() new connections
type ListenService interface {
	// AddressIdentity is the core "identity" part of an address, ex: rsjeuxzlexy4fvo75vrdtj37nrvlmvbw57n5mhypcjpzv3xkka3l4yyd
	AddressIdentity() string

	// AddressFull is the full network address, ex: rsjeuxzlexy4fvo75vrdtj37nrvlmvbw57n5mhypcjpzv3xkka3l4yyd.onion:9878
	AddressFull() string

	Accept() (net.Conn, error)
	Close()
}

// ACN is Anonymous Communication Network implementation wrapper that supports Open for new connections and Listen to accept connections
type ACN interface {
	// GetBootstrapStatus returns an int 0-100 on the percent the bootstrapping of the underlying network is at and an optional string message
	GetBootstrapStatus() (int, string)
	// WaitTillBootstrapped Blocks until underlying network is bootstrapped
	WaitTillBootstrapped()
	// Sets the calback function to be called when ACN status changes
	SetStatusCallback(callback func(int, string))

	// Restarts the underlying connection
	Restart()

	// Open takes a hostname and returns a net.conn to the derived endpoint
	// Open allows a client to resolve various hostnames to connections
	// The supported types are onions address are:
	//  * ricochet:jlq67qzo6s4yp3sp
	//  * jlq67qzo6s4yp3sp
	//  * 127.0.0.1:55555|jlq67qzo6s4yp3sp - Localhost Connection
	Open(hostname string) (net.Conn, string, error)

	// Listen takes a private key and a port and returns a ListenService for it
	Listen(identity PrivateKey, port int) (ListenService, error)

	Close()
}
