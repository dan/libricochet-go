package connectivity

import (
	"fmt"
	"testing"
)

func getStatusCallback(progChan chan int) func(int, string) {
	return func(prog int, status string) {
		fmt.Printf("%v %v\n", prog, status)
		progChan <- prog
	}
}

func TestTorProvider(t *testing.T) {
	progChan := make(chan int)
	acn, err := StartTor(".", "")
	if err != nil {
		t.Error(err)
		return
	}
	acn.SetStatusCallback(getStatusCallback(progChan))

	progress := 0
	for progress < 100 {
		progress = <-progChan
	}

	acn.Close()
}
