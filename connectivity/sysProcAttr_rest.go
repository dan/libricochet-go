// +build !windows

package connectivity

import (
	"syscall"
)

var sysProcAttr = &syscall.SysProcAttr{}
