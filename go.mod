module git.openprivacy.ca/openprivacy/libricochet-go

require (
	github.com/agl/ed25519 v0.0.0-20170116200512-5312a6153412
	github.com/cretz/bine v0.1.1-0.20200124154328-f9f678b84cca
	github.com/golang/protobuf v1.2.0
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20190128193316-c7b33c32a30b
	golang.org/x/net v0.0.0-20190125091013-d26f9f9a57f3 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
)

go 1.13
