package application

import (
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/connection"
)

// Instance is a  concrete instance of a ricochet application, encapsulating a connection
type Instance struct {
	connection.AutoConnectionHandler
	Connection     *connection.Connection
	RemoteHostname string
}

// InstanceFactory generates ApplicationInstances on a specific connection.
type InstanceFactory struct {
	handlerMap map[string]func(*Instance) func() channels.Handler
}

// Init sets up an Application Factory
func (af *InstanceFactory) Init() {
	af.handlerMap = make(map[string]func(*Instance) func() channels.Handler)
}

// AddHandler defines a channel type -> handler construct function
func (af *InstanceFactory) AddHandler(ctype string, chandler func(*Instance) func() channels.Handler) {
	af.handlerMap[ctype] = chandler
}

// GetHandlers returns all handlers
func (af *InstanceFactory) GetHandlers() []string {
	keys := make([]string, len(af.handlerMap))

	i := 0
	for k := range af.handlerMap {
		keys[i] = k
		i++
	}

	return keys
}

// GetHandler returns a set handler for the channel type.
func (af *InstanceFactory) GetHandler(ctype string) func(*Instance) func() channels.Handler {
	return af.handlerMap[ctype]
}

// GetApplicationInstance builds a new application instance using a connection as a base.
func (af *InstanceFactory) GetApplicationInstance(rc *connection.Connection) *Instance {
	rai := new(Instance)
	rai.Init()
	rai.RemoteHostname = rc.RemoteHostname
	rai.Connection = rc
	for t, h := range af.handlerMap {
		rai.RegisterChannelHandler(t, h(rai))
	}
	return rai
}
