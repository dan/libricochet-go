package utils

import (
	"crypto/rand"
	"golang.org/x/crypto/ed25519"
	"math"
	"testing"
)

const (
	privateKeyFile = "./../testing/private_key"
)

func TestLoadPrivateKey(t *testing.T) {
	_, err := LoadPrivateKeyFromFile(privateKeyFile)
	if err != nil {
		t.Errorf("Error while loading private key from file: %v", err)
	}
}

func TestEDH(t *testing.T) {
	cpub, cpriv, _ := ed25519.GenerateKey(rand.Reader)
	spub, spriv, _ := ed25519.GenerateKey(rand.Reader)
	cedh := EDH(cpriv, spub)
	sedh := EDH(spriv, cpub)
	if string(cedh[:]) != string(sedh[:]) {
		t.Errorf("Client and Server should see the same secret %v %v", cedh, sedh)
	}
}

func TestGetRandNumber(t *testing.T) {
	num := GetRandNumber()
	if !num.IsUint64() || num.Uint64() > uint64(math.MaxUint32) {
		t.Errorf("Error random number outside of expected bounds %v", num)
	}
}
